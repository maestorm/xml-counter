import * as vscode from 'vscode';

const xmlFileCountStatusBarItem = vscode.window.createStatusBarItem(
    vscode.StatusBarAlignment.Left,
    1
);

const updateStatusBarItem = async () => {
    const xmlFiles = await vscode.workspace.findFiles('**/*.xml');
    xmlFileCountStatusBarItem.text = `$(file-code) XML: ${xmlFiles.length}`;
    xmlFileCountStatusBarItem.show();
};

export function activate(context: vscode.ExtensionContext) {
    let disposable = vscode.commands.registerCommand('xmlcounter.work', () => {
        vscode.window.showInformationMessage('Counting XML files...');
        updateStatusBarItem();
    });

    context.subscriptions.push(disposable);

    xmlFileCountStatusBarItem.text = '$(file-code) Counting XML Files...';
    xmlFileCountStatusBarItem.show();
    updateStatusBarItem();

    vscode.workspace.onDidChangeWorkspaceFolders(updateStatusBarItem);
    vscode.workspace.onDidCreateFiles(updateStatusBarItem);
    vscode.workspace.onDidDeleteFiles(updateStatusBarItem);
    
    context.subscriptions.push(xmlFileCountStatusBarItem);
}