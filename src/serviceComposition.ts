export type Main_Request = string;
export type Main_Result = string;

export class ServiceComposition {

    constructor(
        private a: ServiceA,
        private b: ServiceB,
        private c: ServiceC,
        private d: ServiceD,
        private e: ServiceE,
        private f: ServiceF,
        private errorMapper: ErrorMapper,
    ){}

    run(req: Main_Request, timeoutMillis: number = 100, cancellation: CancellationToken): Promise<Main_Result> {
      const asyncResultB = async (b: ServiceB) =>
        await new Promise<ServiceB_Result>((resolve, reject) => {
          const onCancelledListener = () => {
            reject(this.errorMapper.aborted());
          };

          cancellation.onCancelled(onCancelledListener);
          
          b.submit(req, () => cancellation.isCancelled(), timeoutMillis, (err, result) => {
            cancellation.onCancelled(() => {});
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
          });
        });
      
      const firstPath = async (a: ServiceA, b: ServiceB, d: ServiceD) => {
          const token = a.start(req);
          const resultA = a.poll(token);
          if (resultA === null) {
            this.errorMapper.aborted();
            return;
          }
          const resultB = await asyncResultB(b);
          return d.merge(resultA, resultB);
      };

      const secondPath = async (e: ServiceE, b: ServiceB, c: ServiceC, f: ServiceF) => {
        const [resultE, cancelTokenE] = await e.transform(asyncResultB(b));
        const resultC = await c.call(req);
        if (cancellation.isCancelled()) {
          cancelTokenE();
          this.errorMapper.aborted();
        }
        const [resultFPromise, cancelTokenF] = await e.combine(Promise.resolve(resultE), c.call(req));
        if (cancellation.isCancelled()) {
          cancelTokenE();
          cancelTokenF();
          this.errorMapper.aborted();
        }
        const resultF = await resultFPromise;

        return f.present(resultC, resultF);
      };

      const thirdPath = async (a: ServiceA, c: ServiceC, e: ServiceE, f: ServiceF) => {
        try {
          // First part: (A + C) -> E
          const tokenA = a.start(req);
          const resultA = a.poll(tokenA) as string;
  
          const resultC = await c.call(req);
  
          if (cancellation.isCancelled()) {
              throw this.errorMapper.aborted();
          }
  
          const [resultE, cancelTokenE] = await e.combine(Promise.resolve(resultA), c.call(req));
  
          if (cancellation.isCancelled()) {
              cancelTokenE();
              throw this.errorMapper.aborted();
          }
  
          // Second part: (((A + C) -> E) + C) -> F
          const [resultFPromise, cancelTokenF] = await e.combine(Promise.resolve(resultE), c.call(req));
  
          if (cancellation.isCancelled()) {
              cancelTokenE();
              cancelTokenF();
              throw this.errorMapper.aborted();
          }
  
          const resultF = await Promise.race([
              resultFPromise,
              new Promise<never>((resolve, reject) => {
                  const timeoutId = setTimeout(() => {
                      reject(this.errorMapper.timedOut());
                  }, timeoutMillis);
  
                  const onCancelledListener = () => {
                      clearTimeout(timeoutId);
                      reject(this.errorMapper.aborted());
                  };
  
                  cancellation.onCancelled(onCancelledListener);
              }),
          ]);
  
          const finalResult = f.present(resultC, resultF);
  
          return finalResult;
        } catch (error) {
            throw this.errorMapper.error([error as Error]);
        }
      };
      
      return new Promise<Main_Result>((resolve, reject) => {
        const timeoutId = setTimeout(() => {
          reject(this.errorMapper.timedOut());
        }, timeoutMillis);

        Promise.all([firstPath(this.a, this.b, this.d), secondPath(this.e, this.b, this.c, this.f), thirdPath(this.a, this.c, this.e, this.f)])
          .then((result) => {
              clearTimeout(timeoutId);
              return result;
          })
          .catch((error) => {
              clearTimeout(timeoutId);
              reject(error);
          });

        // Register cancellation listener
        const onCancelledListener = () => {
            clearTimeout(timeoutId);
            reject(this.errorMapper.aborted());
        };

        cancellation.onCancelled(onCancelledListener);
    });
    }
}

export type ServiceA_InvocationToken = string;
export type ServiceA_Result = string;
export interface ServiceA {
  start(req: Main_Request): ServiceA_InvocationToken;

  /**
   * @param tok obtained from previous call to {@link #start}
   * @return result of invocation identified by Token if it already finished and result has not been collected yet,
   * 			or null if invocation has not finished yet or the result has already been collected.
   */
  poll(tok: ServiceA_InvocationToken): ServiceA_Result | null;
  abort(tok: ServiceA_InvocationToken): void
}


export type ServiceB_Result = string;
export interface ServiceB {

	/**
	 * @param timeout if result is not available within timeout will complete with error
	 * @param isCancelled the implementation may check this to see if caller is still interested in the result,
	 * 				     if this returns true will be completed by error early
	 * @param callback exactly one of Result or Error will be present
	 */
	submit(
    req: Main_Request, 
    isCancelled: () => boolean, 
    timeout: number,
    callback: (err: Error | undefined, result: ServiceB_Result) => void
  ): void;
}

export type ServiceC_Result = string;
export interface ServiceC {

	call(req: Main_Request): Promise<ServiceC_Result>;	
}

export interface ServiceD {

	/**
	 * @return may be completed exceptionally if merging fails
	 */
	merge(a: ServiceA_Result, b: ServiceB_Result): Promise<Main_Result>;	
}

export type ServiceE_Result = string;
export type ServiceE_CancelToken = () => void;
export interface ServiceE {

  /**
   * 
   * @returns future for the result and callback that may be called to abort execution early,
   *          when aborted the future will be rejected by error
   */
  transform(b: Promise<ServiceB_Result>): [Promise<ServiceE_Result>, ServiceE_CancelToken];	
  combine(a: Promise<ServiceA_Result>, c: Promise<ServiceC_Result>): [Promise<ServiceE_Result>, ServiceE_CancelToken];	
}

export interface ServiceF {
  present(c: ServiceC_Result, e: ServiceE_Result): Main_Result;
}

export interface ErrorMapper {
  error(orig: Error[]): Promise<Error>
  timedOut(): Error
  aborted(): Error
}

export interface CancellationToken {
  isCancelled(): boolean
  onCancelled(listener: () => void): void
}
