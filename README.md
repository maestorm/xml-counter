![alt text](icon.png "Title")

## Description

This extension provides a counter of XML files (*.xml) in current workspace displayed in status bar on startup of Visual Studio Code instance.

## Requirements

- installed Visual Studio Code 

No additional dependencies are required.

## Release Notes

### 1.0.0

- Initial release of the XML Counter extension.

---

## More information

- [source code on GitLab](https://gitlab.com/maestorm/xmlcounter.git)
